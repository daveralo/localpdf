# Local PDF

Function to create a PDF document from a HTML template

## Deployment

node -e 'require("./index").init()'

## Built With

* [html-pdf](https://www.npmjs.com/package/html-pdf) - HTML to PDF converter that uses phantomjs
* [handlebars](https://www.npmjs.com/package/handlebars) - Handlebars.js is an extension to the Mustache templating language


## Authors

* **Daniel Vera** 