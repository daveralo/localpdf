var admin = require('firebase-admin');
var fs = require('fs');
var pdf = require('html-pdf');

var options = { height: '333mm',        // allowed units: mm, cm, in, px
                width: '216mm', 
                orientation: 'portrait',
              };
const handlebars = require('handlebars');

var serviceAccount = require('/Users/daveralo/projects/google/insuti-ddeba-firebase-adminsdk-jai1w-97c02988e5.json');

const email = 'daveralo@gmail.com';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

let db = admin.firestore();

module.exports.init = function () {

  let quoteArray = {};
  let user = {};
  const source = fs.readFileSync('./template.html', 'utf8');
  
  db.collection('healthQuotes')
      .get()
      .then(querySnapshot => {
        
        querySnapshot.forEach(function(doc) {
          if (email === doc.data().email){
            user = doc.data();
            const html = handlebars.compile(source)(user);

            pdf.create(html, options).toFile('./output.pdf', function(err, res) {
              if (err) return console.log(err);
              console.log(user); 
              console.log(res); 
            });            
          }
        });
      })
      .catch(function(error) {
        console.log("Error getting documents: ", error);
      }); 
};  